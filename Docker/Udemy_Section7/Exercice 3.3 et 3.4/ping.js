var express = require('express');
var app = express();
app.get('/ping', function(req, res) {
    console.log("received a request");
    res.setHeader('Content-Type', 'text/plain');
    res.end("PONG");
});
app.listen(process.env.PORT || 80);
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() intervalFired = new EventEmitter<number>() //Event qui va emmettre chaque seconde le dernier nombre et qui va etre renvoyé dans app
  interval; //Chaque interval
  lastNumber = 0; // Nombre incrémenter a chaque interval

  constructor() { }

  ngOnInit(): void {
  }

  onStartGame(){
    this.interval =setInterval(()=>{
      this.intervalFired.emit(this.lastNumber+1);
      this.lastNumber++;
    },1000);
  }

  onStopGame(){
    clearInterval(this.interval);
  }
}

let age: number;

age = 23;

let username: string;

username = 'Thomas';

let isStudent: boolean;

isStudent = true;


let hobbies: string[];

hobbies = ['Jeux Vidéos', 'Mangas'];

type Person = {

    name: string;
    age: number;
}

let person : Person;

person = {
    name: 'Thomas',
    age: 23
};

// person = {
//     isStudent: true
// };

let people: {
    name: string;
    age: number;
}[];

let course: string | number = 'React - The Complete Guide';

course = 1234;

function add(a: number, b: number){
    return a + b;
}

function insertAtBeginning<T>(array: T[], value: T){
    const newArray = [value, ...array];
    return newArray;
}

const demoArray = [1,2,3];

const updatedArray = insertAtBeginning(demoArray, -1); // [-1, 1, 2, 3]
const stringArray = insertAtBeginning (['a', 'b', 'c'], 'd'); // ['d', 'a', 'b', 'c']

// updatedArray[0].split('');

class Student {
    firstName: string;
    lastName: string;
    age: number;
    private courses : string[];

    constructor(firstName: string, lastName: string, age: number, courses: string[]){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.courses = courses;
    }

    enroll(courseName: string){
        this.courses.push(courseName);
    }

    listCourses(){
        return this.courses.slice();
    }
}

const student = new Student('Thomas', 'Calvete Ruffier', 23, ['React', 'Angular']);
student.enroll('Vue');

// student.listCourses();

interface Human {
    name: string;
    age: number;

    greet: () => void;
}

let thomas: Human;

thomas = {
    name: 'Thomas',
    age: 23,
    greet(){
        console.log('Bonjour, Je suis' + this.name);
    }
};

class Instructor implements Human {
    name: string;
    age: number;

    constructor(name: string, age: number){
        this.name = name;
        this.age = age;
    }

    greet(){
        console.log('Bonjour, Je suis' + this.name);
    }
}
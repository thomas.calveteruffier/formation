import { Directive ,Renderer2,OnInit, ElementRef, HostListener, HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appBetterhighliht]'
})
export class BetterhighlihtDirective implements OnInit {
  @Input() defaultColor: string = 'transparent';
  @Input() highlightColor: string = 'blue';
  @HostBinding('style.backgroundColor') backgroudColor: string;

  constructor(private elRef : ElementRef,private renderer: Renderer2) { }

  ngOnInit(): void {
    this.backgroudColor = this.defaultColor;
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
  }

  //Quand la souris va aller sur l'élément de la directive
  @HostListener('mouseenter') mouseover( eventDate : Event){
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');
    this.backgroudColor = this.highlightColor;
  }

  //Quand la souris va sortir de l'élément
  @HostListener('mouseleave') mouseleave( eventDate : Event){
    //this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.backgroudColor = this.defaultColor ;
  }
}

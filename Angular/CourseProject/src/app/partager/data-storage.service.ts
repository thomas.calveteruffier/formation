import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { RecetteService } from "../recette/recette.service";
import { Recette } from "../recette/recette.model";
import * as fromApp from '../store/app.reducer';
import * as RecetteActions from '../recette/store/recette.actions';

@Injectable({providedIn: 'root'})
export class DataStorageService {
  constructor(private http: HttpClient, private recetteService: RecetteService, private store: Store<fromApp.AppState>) {}

  storeRecettes() {
    const recettes = this.recetteService.getRecettes();
    this.http.put('https://ng-course-recipe-book-23429-default-rtdb.europe-west1.firebasedatabase.app/recettes.json',recettes)
      .subscribe((response) => {
        console.log(response);
      });
  }

    fetchRecettes() {
      
      return this.http.get<Recette[]>('https://ng-course-recipe-book-23429-default-rtdb.europe-west1.firebasedatabase.app/recettes.json'
        ).pipe(
        map(recettes => {
          return recettes.map(recette => {
            return {...recette, ingredients: recette.ingredients ? recette.ingredients : []};
          });
        }),
        tap(recettes => {
          //this.recetteService.setRecettes(recettes);
          this.store.dispatch(new RecetteActions.SetRecette(recettes));
        }
      )); 
    }

    // fetchRecettes() {
    //     this.http
    //       .get<Recette[]>(
    //         'bdd/recettes.json'
    //       )
    //       .pipe(
    //         map(recettes => {
    //           return recettes.map(recette => {
    //             return {
    //               ...recette,
    //               ingredients: recette.ingredients ? recette.ingredients : []
    //             };
    //           });
    //         })
    //       )
    //       .subscribe(recettes => {
    //         console.log(recettes);
    //         this.recetteService.setRecettes(recettes);
    //       });
    //   }

}
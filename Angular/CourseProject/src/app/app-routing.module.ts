import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthComponent } from './auth/auth.component'

const appRoutes: Routes = [
    { path: '', redirectTo: '/recettes', pathMatch: 'full' },
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {
    initialNavigation: 'enabledBlocking'
})],
    exports: [RouterModule]

})
export class AppRoutingModule{

}
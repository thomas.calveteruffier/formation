import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from '../auth/store/auth.actions';
import { map } from 'rxjs/operators';
import * as RecetteActions from '../recette/store/recette.actions';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy{
    isAthenticated =false;
    private userSub: Subscription;

    constructor(private store:Store<fromApp.AppState> ) {}

    ngOnInit() {
        this.userSub = this.store.select('auth')
        .pipe(map(authState => authState.user))
        .subscribe(user => {
            this.isAthenticated = !!user;
            console.log(!user);
            console.log(!!user);
        });
    }

    onSaveData() {
        // this.dataStorageService.storeRecettes();
        this.store.dispatch(new RecetteActions.StoreRecettes());
    }

    onFetchData() {
        // this.dataStorageService.fetchRecettes().subscribe();
        this.store.dispatch(new RecetteActions.FetchRecette());
    }

    onLogout() {
        this.store.dispatch(new AuthActions.Logout());
    }

    ngOnDestroy() {
        this.userSub.unsubscribe();
    }
}
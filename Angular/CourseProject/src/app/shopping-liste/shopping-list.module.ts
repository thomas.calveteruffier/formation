import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../partager/shared.module";
import { ShoppingEditComponent } from "./shopping-edit/shopping-edit.component";
import { ShoppingListRoutingModule } from "./shopping-list-routing.module";
import { ShoppingListeComponent } from "./shopping-liste.component";

@NgModule({
    declarations: [
        ShoppingListeComponent,
        ShoppingEditComponent,
    ],
    imports: [
        RouterModule,
        ShoppingListRoutingModule,
        FormsModule,
        SharedModule
    ]
})
export class ShoppingListModule { }
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ShoppingListeComponent } from "./shopping-liste.component";

const routes: Routes = [
    { path: 'shopping-list', component: ShoppingListeComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShoppingListRoutingModule { }
import { ActionReducerMap } from '@ngrx/store';

import * as fromShoppingList from '../shopping-liste/store/shopping-list.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import * as fromRecette from '../recette/store/recette.reducer';

export interface AppState {
    shoppingList: fromShoppingList.State;
    auth: fromAuth.State;
    recette: fromRecette.State;
}

export const appReducer: ActionReducerMap<AppState> = {
    shoppingList: fromShoppingList.shoppingListReducer,
    auth: fromAuth.authReducer,
    recette: fromRecette.recetteReducer
};
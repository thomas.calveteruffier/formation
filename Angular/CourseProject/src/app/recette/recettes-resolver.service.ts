import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot  } from "@angular/router";

import { Recette } from "./recette.model";
import { Store } from "@ngrx/store";
import * as fromApp from '../store/app.reducer';
import * as RecetteActions from './store/recette.actions';
import { Actions, ofType } from "@ngrx/effects";
import { map, switchMap, take } from "rxjs/operators";
import { of } from "rxjs";

@Injectable( { providedIn: "root" } )
export class RecettesResolverService implements Resolve<Recette[]> {
  constructor( private store:Store<fromApp.AppState>, private action$: Actions) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('recette').pipe(
      take(1),
      map(recetteState => {
      return recetteState.recette;
    }),
    switchMap(recettes => {
      if (recettes.length === 0) {
        this.store.dispatch(new RecetteActions.FetchRecette());
        return this.action$.pipe(
          ofType(RecetteActions.SET_RECETTE), 
          take(1)
        );
      } else {
        return of(recettes);
      }
    })
    );
  }
}
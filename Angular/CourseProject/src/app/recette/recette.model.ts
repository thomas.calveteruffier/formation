import { Ingredient } from "../partager/ingredient.model";

export class Recette{
    public name: string;//Nom de la recette
    public description: string;// Description de la recette
    public imagePath: string;//Image de la recette avec une Url
    public ingredients: Ingredient[];

    constructor(name: string, description: string, imagePath: string, ingredients: Ingredient[]){
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.ingredients = ingredients;
    }
}
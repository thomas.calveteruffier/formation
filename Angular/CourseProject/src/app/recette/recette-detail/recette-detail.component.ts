import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params,Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Recette } from '../recette.model';
import * as fromApp from '../../store/app.reducer';
import { map, switchMap } from 'rxjs/operators';
import * as RecetteActions from '../store/recette.actions';
import * as ShoppingListAction from '../../shopping-liste/store/shopping-list.actions';

@Component({
  selector: 'app-recette-detail',
  templateUrl: './recette-detail.component.html',
  styleUrls: ['./recette-detail.component.css']
})
export class RecetteDetailComponent implements OnInit {
  recette :Recette;
  id: number;

  constructor( private route: ActivatedRoute, private router: Router, private store: Store<fromApp.AppState>) { }

  ngOnInit(){
    this.route.params.pipe(map(params =>{
      return +params['id'];
    }),
    switchMap(id=>{
      this.id = id;
      return this.store.select('recette');
    }),
    map(recetteState => {
      return recetteState.recette.find((recette, index) => {
        return index === this.id;
      });
    })
    ).subscribe(recette => {
          this.recette = recette;
      });
}

  onEditRecette(){
    this.router.navigate(['edit'], {relativeTo: this.route});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onAddToShoppingList(){
    // this.recetteService.addIngredientsToShoppingList(this.recette.ingredients);
    this.store.dispatch(new ShoppingListAction.AddIngredients(this.recette.ingredients));
  }

  onDeleteRecette(){
    // this.recetteService.deleteRecette(this.id);
    this.store.dispatch(new RecetteActions.DeleteRecette(this.id));
    this.router.navigate(['/recettes']);
  }

}

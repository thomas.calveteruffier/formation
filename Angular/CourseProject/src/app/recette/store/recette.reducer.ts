import { Recette } from "../recette.model";
import * as RecetteActions from './recette.actions';

export interface State {
    recette: Recette[];
}

const initialState: State = {
    recette: []
};

export function recetteReducer(state = initialState, action: RecetteActions.RecetteActions) {
    switch(action.type){
        case RecetteActions.SET_RECETTE:
            return {
                ...state,
                recette: [...action.payload]
            };
        case RecetteActions.ADD_RECETTE:
            return {
                ...state,
                recette: [...state.recette, action.payload]
            };
        case RecetteActions.UPDATE_RECETTE:
            const recette = state.recette[action.payload.index];
            const updatedRecette = {
                ...recette,
                ...action.payload.newRecette
            };
            const recettes = [...state.recette];
            recettes[action.payload.index] = updatedRecette;
            return {
                ...state,
                recette: recettes
            };
        case RecetteActions.DELETE_RECETTE:
            return {
                ...state,
                recette: state.recette.filter((recette, recetteIndex) => {
                    return recetteIndex !== action.payload;
                })
            };
        default:
            return state;
    }
}
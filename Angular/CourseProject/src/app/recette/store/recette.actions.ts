import { Action } from "@ngrx/store";
import { Recette } from "../recette.model";

export const SET_RECETTE = '[Recette] Set Recette';
export const FETCH_RECETTE = '[Recette] Fetch Recette';
export const ADD_RECETTE = '[Recette] Add Recette';
export const UPDATE_RECETTE = '[Recette] Update Recette';
export const DELETE_RECETTE = '[Recette] Delete Recette';
export const STORE_RECETTES = '[Recette] Store Recettes';

export class SetRecette implements Action {
    readonly type = SET_RECETTE;

    constructor(public payload: Recette[]) {}
}

export class FetchRecette implements Action {
    readonly type = FETCH_RECETTE;
}

export class AddRecette implements Action {
    readonly type = ADD_RECETTE;

    constructor(public payload: Recette) {}
}

export class UpdateRecette implements Action {
    readonly type = UPDATE_RECETTE;

    constructor(public payload: {index: number, newRecette: Recette}) {}
}

export class DeleteRecette implements Action {
    readonly type = DELETE_RECETTE;

    constructor(public payload: number) {}
}

export class StoreRecettes implements Action {
    readonly type = STORE_RECETTES;
} 

export type RecetteActions = SetRecette | FetchRecette | AddRecette | UpdateRecette | DeleteRecette | StoreRecettes;
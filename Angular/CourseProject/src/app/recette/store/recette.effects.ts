import { HttpClient } from '@angular/common/http';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, withLatestFrom } from 'rxjs';

import * as RecetteActions from './recette.actions';
import { Recette } from '../recette.model';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';

@Injectable()
export class RecetteEffects {
    @Effect()
    fetchRecette = this.actions$.pipe(
        ofType(RecetteActions.FETCH_RECETTE),
        switchMap(() => {
            return this.http.get<Recette[]>('https://ng-course-recipe-book-23429-default-rtdb.europe-west1.firebasedatabase.app/recettes.json')
        }),
        map(recettes => {
            return recettes.map(recette => {
              return {...recette, ingredients: recette.ingredients ? recette.ingredients : []};
            });
        }),
        map(recettes => {
            return new RecetteActions.SetRecette(recettes);
        })
    );

    @Effect({dispatch: false})
    storeRecettes = this.actions$.pipe(ofType(RecetteActions.STORE_RECETTES),withLatestFrom(this.store.select('recette')) ,switchMap(([actionData, recettesState])=> {
        return this.http.put('https://ng-course-recipe-book-23429-default-rtdb.europe-west1.firebasedatabase.app/recettes.json',recettesState.recette);      
    })
    );


    constructor(private actions$: Actions, private http: HttpClient, private store: Store<fromApp.AppState>) {}
}
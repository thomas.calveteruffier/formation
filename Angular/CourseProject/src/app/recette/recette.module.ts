import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../partager/shared.module";
import { RecetteDetailComponent } from "./recette-detail/recette-detail.component";
import { RecetteEditComponent } from "./recette-edit/recette-edit.component";
import { RecetteItemComponent } from "./recette-liste/recette-item/recette-item.component";
import { RecetteListeComponent } from "./recette-liste/recette-liste.component";
import { RecetteRoutingModule } from "./recette-routing.module";
import { RecetteStartComponent } from "./recette-start/recette-start.component";
import { RecetteComponent } from "./recette.component";

@NgModule({
    declarations: [
        RecetteComponent,
        RecetteListeComponent,
        RecetteDetailComponent,
        RecetteItemComponent,
        RecetteStartComponent,
        RecetteEditComponent
    ],
    imports: [
        RouterModule,
        ReactiveFormsModule, 
        RecetteRoutingModule,
        SharedModule
    ]
})
export class RecetteModule{}
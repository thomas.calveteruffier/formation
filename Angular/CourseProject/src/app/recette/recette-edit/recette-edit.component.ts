import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray } from '@angular/forms';
import { RecetteService } from '../recette.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import { map, Subscription } from 'rxjs';
import * as RecetteActions from '../store/recette.actions';

@Component({
  selector: 'app-recette-edit',
  templateUrl: './recette-edit.component.html',
  styleUrls: ['./recette-edit.component.css']
})
export class RecetteEditComponent implements OnInit, OnDestroy {
  id:number;
  editMode =false;
  recetteForm: FormGroup;

  private storeSub: Subscription;

  constructor(private  route: ActivatedRoute, private router: Router, private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
        //console.log(this.editMode);
      }
    );
  }

  onSubmit() {
    //const nouvelleRecette = new Recette(this.recetteForm.value['name'],this.recetteForm.value['description'],this.recetteForm.value['imagePath'],this.recetteForm.value['ingredients']);
    if(this.editMode) {
      // this.recetteService.updateRecette(this.id, this.recetteForm.value);
      this.store.dispatch(new RecetteActions.UpdateRecette({index: this.id, newRecette: this.recetteForm.value}));
    } else {
      // this.recetteService.addRecette(this.recetteForm.value);
      this.store.dispatch(new RecetteActions.AddRecette(this.recetteForm.value));
    }
    this.onCancel();
  }

  onAddIngredient() {
    (<FormArray>this.recetteForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  ngOnDestroy() {
    if(this.storeSub) {
      this.storeSub.unsubscribe();
    }
  }

  private initForm() {
    let recetteName = '';
    let recetteImagePath = '';
    let recetteDescription = '';
    let recetteIngredients = new FormArray([]);

    if(this.editMode) {
      //const recette = this.recetteService.getRecette(this.id);
      this.storeSub = this.store.select('recette').pipe(map(recetteState => {
        return recetteState.recette.find((recette, index) => {
          return index === this.id;
          })
        })).subscribe(recette => {
          recetteName = recette.name;
          recetteImagePath = recette.imagePath;
          recetteDescription = recette.description;
          if(recette['ingredients']) {
            for(let ingredient of recette.ingredients) {
              recetteIngredients.push(
                new FormGroup({
                'name': new FormControl(ingredient.name , Validators.required),
                'amount': new FormControl(ingredient.amount , [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
                })
              );
            }
          }
        });
      this.recetteForm = new FormGroup({
        'name': new FormControl(recetteName, Validators.required),
        'imagePath': new FormControl(recetteImagePath, Validators.required),
        'description': new FormControl(recetteDescription, Validators.required),
        'ingredients': recetteIngredients
      });
    }
  }

  get controls() { // a getter!
    return (<FormArray>this.recetteForm.get('ingredients')).controls;
  }

  
  onCancel(){
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recetteForm.get('ingredients')).removeAt(index);
  }

}

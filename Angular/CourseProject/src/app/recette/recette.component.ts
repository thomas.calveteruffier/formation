import { Component, OnInit } from '@angular/core';

import { RecetteService } from './recette.service';

@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.css'],
  providers: [RecetteService]
})
export class RecetteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

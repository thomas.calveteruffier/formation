import { Component, Input, OnInit } from '@angular/core';
import { Recette } from '../../recette.model';

@Component({
  selector: 'app-recette-item',
  templateUrl: './recette-item.component.html',
  styleUrls: ['./recette-item.component.css']
})
export class RecetteItemComponent implements OnInit {
  @Input() recetteItem: Recette; 
  @Input() index: number;

  ngOnInit(): void {
  }

}

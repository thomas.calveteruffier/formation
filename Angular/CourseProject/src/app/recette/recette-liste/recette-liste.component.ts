import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { map, Subscription } from 'rxjs';

import { Recette } from '../recette.model';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-recette-liste',
  templateUrl: './recette-liste.component.html',
  styleUrls: ['./recette-liste.component.css']
})
export class RecetteListeComponent implements OnInit, OnDestroy {
  recettes: Recette[];
  subscription: Subscription;

  constructor( private router: Router, private route: ActivatedRoute, private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.subscription = this.store.select('recette').pipe(map(recetteState=> recetteState.recette)).subscribe(
      (recettes: Recette[]) => {
        this.recettes = recettes;
      }
    );
  }

  onNewRecette(){
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

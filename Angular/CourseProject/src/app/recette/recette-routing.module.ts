import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../auth/auth.guard";
import { RecetteDetailComponent } from "./recette-detail/recette-detail.component";
import { RecetteEditComponent } from "./recette-edit/recette-edit.component";
import { RecetteStartComponent } from "./recette-start/recette-start.component";
import { RecetteComponent } from "./recette.component";
import { RecettesResolverService } from "./recettes-resolver.service";

const routes: Routes = [
    { path: 'recettes', component: RecetteComponent,canActivate: [AuthGuard], children:[
        { path: '', component: RecetteStartComponent },
        { path: 'new', component: RecetteEditComponent},
        { path: ':id', component: RecetteDetailComponent , resolve: [RecettesResolverService]},
        { path: ':id/edit', component: RecetteEditComponent,resolve: [RecettesResolverService]},
    ] },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecetteRoutingModule { }
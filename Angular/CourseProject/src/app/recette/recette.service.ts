import { Injectable } from "@angular/core";

import { Ingredient } from "../partager/ingredient.model";
import { Recette } from "./recette.model";
import { Subject } from "rxjs";
import { Store } from "@ngrx/store";
import * as ShoppingListActions from '../shopping-liste/store/shopping-list.actions';
import * as fromApp from '../store/app.reducer';

@Injectable()
export class RecetteService {
  recettesChanged = new Subject<Recette[]>();
  private recettes: Recette[] =[];
  // private recettes: Recette[] = [
  //   new Recette('Escalope à la viennoise','Une bonne escalope','https://assets.afcdn.com/recipe/20190108/85686_w1024h576c1cx1421cy3684.webp',[new Ingredient('viande', 1),new Ingredient('Patate',5)]),
  //   new Recette('Burger','Un bon gros Burger','https://www.umamiburgerparis.com/media/images/minigal/02.jpg',[new Ingredient('Pain',1), new Ingredient('Steak',2)]),   
  // ];

  constructor(
    private store: Store<fromApp.AppState>) { }

  setRecettes(recettes: Recette[]){
    this.recettes = recettes;
    this.recettesChanged.next(this.recettes.slice());
    console.log(this.recettes);
  }

  getRecettes(){
    return this.recettes.slice();
  }

  getRecette(index: number){
    return this.recettes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]){
    // this.shoppinglistService.addIngredients(ingredients);
    this.store.dispatch(new ShoppingListActions.AddIngredients(ingredients));
  }

  addRecette(recette: Recette){
    this.recettes.push(recette);
    this.recettesChanged.next(this.recettes.slice());
  }

  updateRecette(index: number, newRecette: Recette){
    this.recettes[index] = newRecette;
    this.recettesChanged.next(this.recettes.slice());
  }

  deleteRecette(index: number){
    this.recettes.splice(index,1);
    this.recettesChanged.next(this.recettes.slice());
  }
}
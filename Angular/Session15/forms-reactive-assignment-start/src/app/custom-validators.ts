import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

export class CustomValidators {
  static forbiddenProjectNames(control: FormControl): { [s: string]: boolean } {
    if (control.value === 'Test') {
      return { 'nameIsForbidden': true };
    }
    return null;
  }

  static asyncForbiddenProjectNames(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'TestProjet') {
          resolve({ 'nameIsForbidden': true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }
}
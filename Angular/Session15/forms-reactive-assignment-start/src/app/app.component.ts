import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  projetForm: FormGroup;

  ngOnInit() {
    this.projetForm = new FormGroup({
      'nomProjet' : new FormControl(null, [Validators.required, CustomValidators.forbiddenProjectNames], CustomValidators.asyncForbiddenProjectNames),
      'email' : new FormControl(null, [Validators.required, Validators.email]),
      'status' : new FormControl('critical')
    });
  }


  onSaveProject() {
    console.log(this.projetForm.value);
  }
}

export class CounterService{
    activeToInactiveCounter = 0;
    inactiveToActiveCounter = 0;

    incrementInactiveToActive(){
        this.activeToInactiveCounter++;
        console.log(this.activeToInactiveCounter);
    }

    incrementActiveToInactive(){
        this.inactiveToActiveCounter++;
        console.log(this.inactiveToActiveCounter);
    }
}